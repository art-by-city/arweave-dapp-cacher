import 'dotenv/config'
import knex from 'knex'

import { CONFIG, createLogger } from '../util'

const JOB_NAME = 'init-db'
const log = createLogger(JOB_NAME)
const db = knex({
  client: 'pg',
  connection: {
    user: CONFIG.DB_USER,
    database: CONFIG.DB_NAME,
    password: CONFIG.DB_PASS,
    host: CONFIG.DB_HOST,
    port: CONFIG.DB_PORT
  }
})

export async function up() {
  await db.schema
    .createSchemaIfNotExists(CONFIG.JOBS_SCHEMA_NAME)
    .withSchema(CONFIG.JOBS_SCHEMA_NAME)
    .createTable('job_runs', table => {
      table.increments('id', { primaryKey: true })
      table.text('job_name')
      table.enum('result', ['ok', 'fail'])
      table.text('cursor')
      table.jsonb('payload')
      table.jsonb('error')
      table
        .timestamp('created_at', { precision: 6, useTz: true })
        .defaultTo(db.fn.now(6))
      table
        .timestamp('updated_at', { precision: 6, useTz: true })
        .defaultTo(db.fn.now(6))
      table
        .timestamp('finished_at', { precision: 6, useTz: true })

      table.index('cursor', 'index_job_runs_cursor', 'HASH')
    })

  await db.schema
    .createSchemaIfNotExists(CONFIG.DB_SCHEMA)
    .withSchema(CONFIG.DB_SCHEMA)
    .createTable('transactions', table => {
      // const indices = JSON.parse(process.env.INDICES || '[]')

      table.string('id', 64).notNullable()
      table.text('owner')
      table.jsonb('tags')
      table.string('target', 64)
      table.text('quantity')
      table.text('reward')
      table.text('signature')
      table.text('last_tx')
      table.bigInteger('data_size')
      table.string('content_type')
      table.integer('format', 2)
      table.integer('height', 4)
      table.string('owner_address')
      table.string('data_root', 64)
      table.string('parent', 64)
      table.string('block', 64).defaultTo('')
      table.string('bundledIn', 64).defaultTo('')
      table.timestamp('created_at').defaultTo(new Date().toISOString())

      // for (const index of indices) {
      //   table.string(index, 64)
      //   table.index(index, `index_${index}_transactions`, 'HASH')
      // }

      table.primary(['id'], 'pkey_transactions')
      table.index(['height'], 'transactions_height', 'HASH')
      table.index(['owner_address'], 'transactions_owner_address', 'HASH')
      table.index(['target'], 'transactions_target', 'HASH')
    })
}

export async function down() {
  await db.schema
    .dropSchemaIfExists(CONFIG.DB_SCHEMA, true)
    .dropSchemaIfExists(CONFIG.JOBS_SCHEMA_NAME, true)
}
