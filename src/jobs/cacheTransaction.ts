import 'dotenv/config'
import { Storage } from '@google-cloud/storage'
import { EventFunction } from '@google-cloud/functions-framework'
import axios from 'axios'
import sharp from 'sharp'
import { Bundle } from 'arbundles'

import {
  CONFIG,
  createErrorLogger,
  createLogger,
  parseDataUrlForMimeType
} from '../util'

const JOB_NAME = 'cache-transaction'
const log = createLogger(JOB_NAME)
const logError = createErrorLogger(JOB_NAME)

const gcpBaseOpts: { projectId: string } = { projectId: CONFIG.GCP_PROJECT }
const storageConfig = CONFIG.STORAGE_URL
  ? { ...gcpBaseOpts, apiEndpoint: CONFIG.STORAGE_URL }
  : { ...gcpBaseOpts }
const storage = new Storage(storageConfig)

const animatedImageTypes = [
  'image/apng',
  'image/avif',
  'image/gif',
  'image/webp'
]

const baseUrl = `${CONFIG.AR_PROTOCOL}://${CONFIG.AR_HOST}:${CONFIG.AR_PORT}`

export const createHandler = (
  storage: Storage
): EventFunction => async ({ data }: any) => {
  log('Start')
  const { cacheV0Artwork, cacheBundleArtwork } = setup(storage)
  const { id, type } = JSON.parse(Buffer.from(data, 'base64').toString())
  log('Got txid', id)

  const url = `${baseUrl}/${id}`
  if (type === 'application/json') {
    // tx is expected to be artwork json manifest
    log('Fetching', url)
    const res = await axios.get(url)
    const artwork = res.data
    // NB: only cache v0 artwork
    if (!artwork.version) {
      log('Processing v0 artwork')
      await cacheV0Artwork(id, artwork)
    }
  } else if (type === 'binary') {
    // tx is expected to be a binary bundle
    log('Fetching', url)
    const res = await axios.get(url, { responseType: 'arraybuffer' })
    const buffer = res.data as Buffer
    const bundle = new Bundle(buffer)
    log('Processing bundled artwork')
    await cacheBundleArtwork(id, bundle)
  } else {
    logError('Unknown Type', type)
  }

  log('Successfully cached', id)

  return
}

function setup(storage: Storage) {
  const bucket = storage.bucket(CONFIG.BUCKET)

  async function cacheV0Artwork(id: string, artwork: any): Promise<void> {
    const images = (artwork.images as []).map((image: any, idx) => {
      return {
        animated: animatedImageTypes.includes(
          parseDataUrlForMimeType(image.dataUrl)
        ) ? true : undefined,
        image: `${id}-${idx}-image`,
        preview: `${id}-${idx}-preview`,
        preview4k: `${id}-${idx}-preview4k`
      }
    })
    const manifest = {
      ...artwork,
      images
    }

    log('Saving manifest')
    await bucket.file(id).save(JSON.stringify(manifest), {
      contentType: 'application/json',
      gzip: 'auto'
    })

    log('Saving images')
    await Promise.all(images.map(async (image, idx) => {
      const contentType = parseDataUrlForMimeType(artwork.images[idx].dataUrl)
      const imageData = artwork.images[idx].dataUrl.split(',')[1]
      const imageBuffer = Buffer.from(imageData, 'base64')
      const resizeOpts = { fit: sharp.fit.inside, withoutEnlargement: true }
      const flattenOpts = { background: '#FFFFFF' }
      const previewBuffer = await sharp(imageBuffer)
        .resize(1920, 1080, resizeOpts)
        .flatten(flattenOpts)
        .toFormat('jpeg')
        .toBuffer()
      const preview4kBuffer = await sharp(imageBuffer)
        .resize(3840, 2160, resizeOpts)
        .flatten(flattenOpts)
        .toFormat('jpeg')
        .toBuffer()
      try {
        log('Saving', contentType, image.image)
        await bucket.file(image.image).save(imageBuffer, {
          contentType, gzip: 'auto', resumable: false
        })
        log('Saving', image.preview)
        await bucket.file(image.preview).save(previewBuffer, {
          contentType: 'image/jpeg', gzip: 'auto', resumable: false
        })
        log('Saving', image.preview4k)
        await bucket.file(image.preview4k).save(preview4kBuffer, {
          contentType: 'image/jpeg', gzip: 'auto', resumable: false
        })
      } catch (err: any) {
        logError('Error uploading to storage bucket', err.response)
      }
    }))
  }

  async function cacheBundleArtwork(id: string, bundle: Bundle): Promise<void> {
    await Promise.all(bundle.items.map(async item => {
      try {
        log('Saving', item.id)
        const contentType = item.tags.find(
          tag => tag.name === 'Content-Type'
        )?.value || 'auto'
        await bucket.file(item.id).save(item.rawData, {
          contentType, gzip: 'auto', resumable: false
        })
      } catch (err: any) {
        logError('Error uploading to storage bucket', err.response)
      }
    }))

    log('Saving bundle placeholder')
    await bucket.file(id).save(Buffer.from(id), {
      contentType: 'application/octet-stream', resumable: false
    })
  }

  return {
    cacheV0Artwork,
    cacheBundleArtwork
  }
}

export default createHandler(storage)
