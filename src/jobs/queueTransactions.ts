import 'dotenv/config'
import { Firestore, Settings, Timestamp } from '@google-cloud/firestore'
import { Storage } from '@google-cloud/storage'
import { PubSub } from '@google-cloud/pubsub'
import { EventFunction } from '@google-cloud/functions-framework'
import Arweave from 'arweave/node'
import ArDB from 'ardb'
import { LOGS } from 'ardb/lib/utils/log'
import ArdbTransaction from 'ardb/lib/models/transaction'

import { CONFIG, createLogger, createErrorLogger } from '../util'

const JOB_NAME = 'queue-transactions'
const log = createLogger(JOB_NAME)
const logError = createErrorLogger(JOB_NAME)

const gcpBaseOpts: { projectId: string } = { projectId: CONFIG.GCP_PROJECT }
const firestoreOpts: Settings = { ...gcpBaseOpts }
const pubsubOpts = CONFIG.PUBSUB_URL
  ? { ...gcpBaseOpts, apiEndpoint: CONFIG.PUBSUB_URL }
  : { ...gcpBaseOpts }
const storageOpts = CONFIG.STORAGE_URL
  ? { ...gcpBaseOpts, apiEndpoint: CONFIG.STORAGE_URL }
  : { ...gcpBaseOpts }

if (!CONFIG.isProduction) {
  firestoreOpts.host = CONFIG.DB_HOST
  firestoreOpts.port = CONFIG.DB_PORT
  firestoreOpts.ssl = CONFIG.DB_SSL
}

log('using firestore opts', firestoreOpts)
const firestore = new Firestore(firestoreOpts)
log('using pubsub opts', pubsubOpts)
const pubsub = new PubSub(pubsubOpts)
log('using storage opts', storageOpts)
const storage = new Storage(storageOpts)
const arweave = new Arweave({
  protocol: CONFIG.AR_PROTOCOL,
  host: CONFIG.AR_HOST,
  port: CONFIG.AR_PORT
})

export const createHandler = (
  firestore: Firestore,
  storage: Storage,
  pubsub: PubSub,
  arweave: Arweave
): EventFunction => async () => {
  log('Start')
  const {
    fetchCursor,
    fetchUncachedTxIds,
    flushAndClosePubSub,
    publishTxCacheRequestMessage,
    storeNextCursor
  } = setupApis(firestore, pubsub, arweave)

  const cursor = await fetchCursor()
  const { txIds, nextCursor } = await fetchUncachedTxIds(cursor)

  log('got txids', txIds.length)
  for (let i = 0; i < txIds.length; i++) {
    log('got tx',i, txIds[i])
  }

  let error: boolean = false
  await Promise.all(txIds.map(async (id) => {
    if (error) {
      return
    }
    log('got tx', id)
    const [keyExists] = await storage.bucket(CONFIG.BUCKET).file(id).exists()
    log(`Storage ${id} exists?`, keyExists)

    if (!keyExists) {
      await publishTxCacheRequestMessage(id)
    }
    return
  }))

  await flushAndClosePubSub()

  if (nextCursor && !error) {
    await storeNextCursor(nextCursor)
  }

  return
}

function setupApis(
  firestore: Firestore,
  pubsub: PubSub,
  arweave: Arweave
) {
  const collection = firestore.collection(
    `${CONFIG.JOBS_COLLECTION}.${JOB_NAME}.cursors`
  )
  const topic = pubsub.topic(CONFIG.PUBSUB_TOPIC, {
    batching: { maxMessages: CONFIG.QUEUE_LIMIT }
  })
  const ardb = new ArDB(arweave, LOGS.ARWEAVE)

  async function fetchCursor(): Promise<number | undefined> {
    log('Fetching cursor')
    const lastRun = await collection.orderBy('timestamp', 'desc').limit(1).get()

    let cursor
    if (!lastRun.empty) {
      ({ cursor } = lastRun.docs[0].data())
    }
    log('Got cursor', cursor)

    return cursor
  }

  async function fetchUncachedTxIds(
    cursor?: number
  ): Promise<{ nextCursor?: number, txIds: string[] }> {
    log('Fetching tx ids')
    let query = ardb
      .search('transactions')
      .appName(CONFIG.AR_APP_NAME)
      .tag('Bundle-Format', 'binary')
      .tag('Bundle-Version', '2.0.0')
      .tag('Category', 'artwork:bundle')
      .limit(CONFIG.QUEUE_LIMIT)

    if (cursor) {
      query = query.min(cursor)
    }

    const results = await query.find({ sort: 'HEIGHT_ASC' })
    const transactions = results as ArdbTransaction[]
    log('Got artwork bundle tx ids', transactions.length)

    const nextCursor = transactions.length < 1
      ? undefined
      : transactions[transactions.length - 1].block.height + 1

    return {
      txIds: transactions.map(tx => tx.id),
      nextCursor
    }
  }

  async function publishTxCacheRequestMessage(id: string): Promise<boolean> {
    log('Publishing cache request message for', id)
    try {
      const messageId = await topic.publishMessage({
        json: { id, type: 'binary' }
      })
      log(`Published message ${messageId} to topic ${CONFIG.PUBSUB_TOPIC}`)
      return true
    } catch (err: any) {
      logError(err)
      return false
    }
  }

  async function storeNextCursor(nextCursor: number): Promise<void> {
    log('Storing next cursor', nextCursor)
    await collection.add({
      timestamp: Timestamp.fromDate(new Date()),
      cursor: nextCursor
    })
  }

  async function flushAndClosePubSub(): Promise<void> {
    console.log('flush topic and close pubsub')
    await topic.flush()
    // await pubsub.close()
  }

  return {
    fetchCursor,
    fetchUncachedTxIds,
    flushAndClosePubSub,
    publishTxCacheRequestMessage,
    storeNextCursor
  }
}

export default createHandler(firestore, storage, pubsub, arweave)
