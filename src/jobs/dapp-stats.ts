import 'dotenv/config'
import { EventFunction } from '@google-cloud/functions-framework'
import Arweave from 'arweave/node'
import { ApiConfig } from 'arweave/node/lib/api'
import ArDB from 'ardb'
import { LOGS } from 'ardb/lib/utils/log'
import ArdbTransaction from 'ardb/lib/models/transaction'
import _ from 'lodash'
import knex, { Knex } from 'knex'

import { CONFIG, createLogger, createErrorLogger } from '../util'

const JOB_NAME = 'dapp-stats'
const log = createLogger(JOB_NAME)
const logError = createErrorLogger(JOB_NAME)

const arweaveOpts: ApiConfig = {
  protocol: CONFIG.AR_PROTOCOL,
  host: CONFIG.AR_HOST,
  port: CONFIG.AR_PORT
}
log('using arweave opts', arweaveOpts)
const arweave = new Arweave(arweaveOpts)
const db = knex({
  client: 'pg',
  connection: {
    user: CONFIG.DB_USER,
    database: CONFIG.DB_NAME,
    password: CONFIG.DB_PASS,
    host: CONFIG.DB_HOST,
    port: CONFIG.DB_PORT
  }
})

export const createHandler = (
  arweave: Arweave,
  db: Knex
): EventFunction => async () => {
  log('Start')

  const {
    fetchDappTransactions,
    insertTransactions,
    fetchLastJobRun,
    insertJobRun,
    finalizeJobRun
  } = setupApis(arweave, db)

  let cursor: undefined | string, isComplete = false
  const allTransactions: ArdbTransaction[] = []

  const lastRun = await fetchLastJobRun()
  log('Last run', lastRun)

  if (lastRun && lastRun.cursor) {
    cursor = lastRun.cursor
  }

  const [ thisRun ] = await insertJobRun()
  log('This run', thisRun)

  while (!isComplete) {
    const {
      nextCursor,
      transactions
    } = await fetchDappTransactions(cursor)

    allTransactions.push(...transactions)

    if (nextCursor) {
      cursor = nextCursor
    }

    if (!nextCursor || allTransactions.length >= CONFIG.QUEUE_LIMIT) {
      isComplete = true
    }
  }

  if (allTransactions.length > 0) {
    log(`Caching ${allTransactions.length} transactions`)
    await insertTransactions(allTransactions)
  } else {
    log('No transactions to cache')
  }

  const payload = JSON.stringify(allTransactions.map(tx => tx.id))
  await finalizeJobRun(thisRun.id, payload, cursor)

  log('Done')

  return
}

function setupApis(arweave: Arweave, db: Knex) {
  const ardb = new ArDB(arweave, LOGS.ARWEAVE)

  async function fetchDappTransactions(
    cursor?: string
  ): Promise<{ nextCursor: string, transactions: ArdbTransaction[] }> {
    log('Querying dapp transactions')

    let query = ardb.search('transactions')
      .appName(CONFIG.AR_APP_NAME)
      .limit(CONFIG.QUEUE_LIMIT)

    if (CONFIG.AR_APP_VERSION) {
      query = query.tag('App-Version', CONFIG.AR_APP_VERSION)
    }

    if (cursor) {
      log('Using cursor', cursor)
      query = query.cursor(cursor)
    }

    const results = await query.find({ sort: 'HEIGHT_ASC' })
    const nextCursor = query.getCursor()
    const transactions = results as ArdbTransaction[]

    log('Got dapp transactions', transactions.length)
    log('next cursor', nextCursor)

    return { nextCursor, transactions }
  }

  async function insertTransactions(transactions: ArdbTransaction[]) {
    const formattedTransactions = transactions.map(tx => {
      return {
        id: tx.id,
        owner: tx.owner.key,
        tags: JSON.stringify(tx.tags),
        target: tx.recipient,
        quantity: tx.quantity.winston,
        reward: tx.fee.winston,
        signature: tx.signature,
        last_tx: tx.anchor,
        data_size: tx.data.size,
        content_type: tx.data.type,
        height: tx.block.height,
        owner_address: tx.owner.address,
        parent: tx.parent?.id || undefined,
        block: tx.block.id,
        created_at: new Date(tx.block.timestamp*1000).toISOString()
        // bundledIn: '', TODO
      }
    })

    await db
      .withSchema(CONFIG.DB_SCHEMA)
      .table('transactions')
      .insert(formattedTransactions)
      .onConflict()
      .ignore()
  }

  async function fetchLastJobRun() {
    return await db
      .withSchema(CONFIG.JOBS_SCHEMA_NAME)
      .first('id', 'cursor')
      .from('job_runs')
      .where('result', '=', 'ok')
      .orderBy('created_at', 'desc')
  }

  async function insertJobRun() {
    return await db
      .withSchema(CONFIG.JOBS_SCHEMA_NAME)
      .into('job_runs')
      .insert({ job_name: JOB_NAME })
      .returning('id')
  }

  async function finalizeJobRun(id: number, payload: string, cursor?: string) {
    return await db
      .withSchema(CONFIG.JOBS_SCHEMA_NAME)
      .into('job_runs')
      .where('id', '=', id)
      .update({
        cursor,
        result: 'ok',
        payload,
        updated_at: db.fn.now(),
        finished_at: db.fn.now()
      })
  }

  return {
    fetchDappTransactions,
    insertTransactions,
    fetchLastJobRun,
    insertJobRun,
    finalizeJobRun
  }
}

export default createHandler(arweave, db)
